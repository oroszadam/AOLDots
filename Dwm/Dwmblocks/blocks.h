//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		             /*Update Interval*/   /*Update Signal*/
	{"", "/home/adam/Scripts/dwmbar_mpd",    1,		               3},
	{"", "/home/adam/Scripts/micmute",       -1,		           2},
	{"", "/home/adam/Scripts/dwmbar_volume", -1,		           1},
	{"", "/home/adam/Scripts/dwmbar_clock",  60,	               0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
/* static char delim[] = "^f20^"; */
/* static unsigned int delimLen = 6; */
static char delim[] = "^f20^";
static unsigned int delimLen = 6;
