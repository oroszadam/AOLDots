/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx		  	= 3;        /* border pixel of windows */
static const int user_bh					= 25;
/* static const int gappx					    = 12; */
static const unsigned int snap			    = 32;       /* snap pixel */
static const unsigned int gappih            = 10;       /* horiz inner gap between windows */
static const unsigned int gappiv            = 10;       /* vert inner gap between windows */
static const unsigned int gappoh            = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov            = 10;       /* vert outer gap between windows and screen edge */
static const int smartgaps                  = 0;        /* 1 means no outer gap when there is only one window */
static const unsigned int swallowfloating	= 0;
static const unsigned int systraypinning	= 0;
static const unsigned int systrayspacing	= 2;
static const int systraypinningfailfirst	= 1;
static const int showsystray			    = 1;
static const int showbar				    = 1;        /* 0 means no bar */
static const int topbar					    = 1;        /* 0 means bottom bar */
static const int vertpad                    = 10;
static const int sidepad                    = 10;
static const char *fonts[]				    = { "SauceCodePro Nerd Font Mono:size=11", "UbuntuMono Nerd Font Mono:size=10" };

static char normbgcolor[] 				    = "#1e1e2e";
static char normbordercolor[]			    = "#1e1e2e";
static char normfgcolor[]		    		= "#c3bac6";
static char selfgcolor[]		  	      	= "#d9e0ee";
static char selbgcolor[]		  	   	    = "#1e1e2e";
static char selbordercolor[]  		        = "#96cdfb";

static char tag_highlight[]                 = "#2f2f47";
static char tag_1[]                         = "#f5c2e7";
static char tag_2[]                         = "#e8a2af";
static char tag_3[]                         = "#f28fad";
static char tag_4[]                         = "#f8bd96";
static char tag_5[]                         = "#fae3b0";
static char tag_6[]                         = "#abe9b3";
static char tag_7[]                         = "#b5e8e0";
static char tag_8[]                         = "#96cdfb";
static char tag_9[]                         = "#89dceb";

static const unsigned int upadding          = 5;
static const unsigned int ulinepad          = 5;    /* horizontal padding between the underline and tag */
static const unsigned int ulinestroke       = 2;    /* thickness / height of the underline */
static const unsigned int ulinevoffset      = 0;    /* how far above the bottom of the bar the line should appear */
static const int ulineall                   = 0;    /* 1 to show underline on all tags, 0 for just the active ones */

static const char *colors[][3]            = {
	/*               fg         bg         border   */
    [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
    [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};

static const char *customcolors[][2]      = {
    { tag_1, tag_highlight },
    { tag_2, tag_highlight },
    { tag_3, tag_highlight },
    { tag_4, tag_highlight },
    { tag_5, tag_highlight },
    { tag_6, tag_highlight },
    { tag_7, tag_highlight },
    { tag_8, tag_highlight },
    { tag_9, tag_highlight },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	 /* class       instance  title							               tags mask    isfloating  isterminal   noswallow  monitor */
	 { "st",        NULL,     NULL,							               0,           0,          1,           0,         -1 },
	 { "alacritty", NULL,     NULL,							               0,           0,          1,           0,         -1 },
	 { NULL,        NULL,     "Event Tester",				               0,           1,          0,           1,         -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

ResourcePref resources[] = {
		{ "normbgcolor",        STRING,  &normbgcolor },
		{ "normbordercolor",    STRING,  &normbordercolor },
		{ "normfgcolor",        STRING,  &normfgcolor },
		{ "selbgcolor",         STRING,  &selbgcolor },
		{ "selbordercolor",     STRING,  &selbordercolor },
		{ "selfgcolor",         STRING,  &selfgcolor },
		{ "tag_highlight",      STRING,  &tag_highlight },
		{ "tag_1",              STRING,  &tag_1 },
		{ "tag_2",              STRING,  &tag_2 },
		{ "tag_3",              STRING,  &tag_3 },
		{ "tag_4",              STRING,  &tag_4 },
		{ "tag_5",              STRING,  &tag_5 },
		{ "tag_6",              STRING,  &tag_6 },
		{ "tag_7",              STRING,  &tag_7 },
		{ "tag_8",              STRING,  &tag_8 },
		{ "tag_9",              STRING,  &tag_9 },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
/* static const char *dmenucmd[]          = { "rofi", "-modi", "combi,drun", "-combi-modi", "ssh,run,drun", "-theme", "/home/adam/.config/onedark.rasi", "-show", "combi", "-run-shell-command", "{cmd}", NULL }; */
static const char *dmenucmd[]          = { "dmenu_run", "-x", "10", "-y", "10", "-w", "1900", NULL };
static const char *termcmd[]           = { "alacritty", NULL };
static const char *browsercmd[]        = { "firefox-bin", NULL };
static const char *passcmd[]           = { "passmenu", "--type", NULL };
static const char *musictogglecmd[]    = { "/home/adam/Scripts/mpc_dwmblocks", "toggle", NULL };
static const char *musicnextcmd[]      = { "/home/adam/Scripts/mpc_dwmblocks", "next", NULL };
static const char *musicprevcmd[]      = { "/home/adam/Scripts/mpc_dwmblocks", "prev", NULL };
static const char *musicvolupcmd[]     = { "mpc", "volume", "+5", NULL };
static const char *musicvoldowncmd[]   = { "mpc", "volume", "-5", NULL };
static const char *filemanagercmd[]    = { "pcmanfm", NULL };
static const char *voldowncmd[]        = { "/home/adam/Scripts/volumectl", "-", NULL };
static const char *volupcmd[]          = { "/home/adam/Scripts/volumectl", "+", NULL };
static const char *mutecmd[]           = { "/home/adam/Scripts/volumectl", "mute", NULL };
static const char *micmutecmd[]        = { "/home/adam/Scripts/volumectl", "micmute", NULL };
static const char *playbacktogglecmd[] = { "timeout", "0.1", "playerctl", "-a", "play-pause", NULL };
static const char *musiccmd[]          = { "alacritty", "-e", "ncmpcpp", NULL };
static const char *mousecmd[]          = { "keym", NULL };
static const char *screenshotcmd[]     = { "flameshot", "gui", NULL };
static const char *anicmd[]            = { "st", "-e", "ani-cli", NULL };
static const char *vpncmd[]            = { "/home/adam/Scripts/connectvpn", NULL };
static const char *autovpncmd[]        = { "/home/adam/Scripts/connectvpn", "auto", NULL };
static const char *flatcmd[]           = { "/home/adam/Scripts/flatrun", "auto", NULL };
static const char *themecmd[]           = { "/home/adam/Scripts/aoldots", "theme", "menu", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY|ShiftMask,             XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_g,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ControlMask,           XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ShiftMask,             XK_e,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY,                       XK_f,      togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = +1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = -1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
	{ MODKEY,       				XK_p,      spawn,			{.v = dmenucmd} },
	{ MODKEY|ShiftMask,				XK_Return, spawn,			{.v = termcmd} },
	{ MODKEY,       				XK_b,      spawn,			{.v = browsercmd} },
	{ MODKEY|ControlMask,   		XK_p,      spawn,			{.v = passcmd} },
	{ MODKEY|ShiftMask,				XK_p,      spawn,			{.v = musictogglecmd} },
	{ MODKEY,       				XK_c,      spawn,			{.v = musicnextcmd} },
	{ MODKEY,       				XK_z,      spawn,			{.v = musicprevcmd} },
	{ MODKEY|ShiftMask,				XK_c,      spawn,			{.v = musicvolupcmd} },
	{ MODKEY|ShiftMask,				XK_z,      spawn,			{.v = musicvoldowncmd} },
	{ MODKEY|ShiftMask,				XK_f,      spawn,			{.v = filemanagercmd} },
	{ MODKEY,       				XK_F2,     spawn,			{.v = voldowncmd} },
	{ MODKEY,       				XK_F3,     spawn,			{.v = volupcmd} },
	{ MODKEY,        				XK_F4,     spawn,			{.v = mutecmd} },
	{ MODKEY|ShiftMask,				XK_m,      spawn,			{.v = micmutecmd} },
	{ MODKEY|Mod1Mask,  			XK_p,      spawn,			{.v = playbacktogglecmd} },
	{ MODKEY,       				XK_F1,     spawn,			{.v = musiccmd} },
	{ MODKEY,				        XK_n,      spawn,			{.v = mousecmd} },
	{ MODKEY|ShiftMask,		        XK_s,      spawn,			{.v = screenshotcmd} },
	{ MODKEY,       		        XK_a,      spawn,			{.v = anicmd} },
	{ MODKEY|ShiftMask,		        XK_v,      spawn,			{.v = autovpncmd} },
	{ MODKEY,       		        XK_v,      spawn,			{.v = vpncmd} },
	{ MODKEY|Mod1Mask,       	    XK_f,      spawn,			{.v = flatcmd} },
	{ MODKEY|ShiftMask,       	    XK_t,      spawn,			{.v = themecmd} },
    { MODKEY|Mod1Mask,              XK_r,      quit,            {1} },
    { MODKEY,                       XK_bracketleft, moveclientX, {.i = -20} },
    { MODKEY,                       XK_bracketright, moveclientX, {.i = 20} },
    { MODKEY|ShiftMask,             XK_bracketleft, moveclientY, {.i = -20} },
    { MODKEY|ShiftMask,             XK_bracketright, moveclientY, {.i = 20} },
    { MODKEY|ControlMask,           XK_bracketleft, resizeclientX, {.i = -20} },
    { MODKEY|ControlMask,           XK_bracketright, resizeclientX, {.i = 20} },
    { MODKEY|ShiftMask|ControlMask, XK_bracketleft, resizeclientY, {.i = -20} },
    { MODKEY|ShiftMask|ControlMask, XK_bracketright, resizeclientY, {.i = 20} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                  event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
    { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
    { ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
    { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
    { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
    { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
    { ClkTagBar,            0,              Button1,        view,           {0} },
    { ClkTagBar,            0,              Button3,        toggleview,     {0} },
    { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
    { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

static const char *ipcsockpath = "/tmp/dwm.sock";
static IPCCommand ipccommands[] = {
  IPCCOMMAND(  view,                1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  toggleview,          1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  tag,                 1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  toggletag,           1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  tagmon,              1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  focusmon,            1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  focusstack,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  zoom,                1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  incnmaster,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  killclient,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  togglefloating,      1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  setmfact,            1,      {ARG_TYPE_FLOAT}  ),
  IPCCOMMAND(  setlayoutsafe,       1,      {ARG_TYPE_PTR}    ),
  IPCCOMMAND(  togglebar,           1,      {ARG_TYPE_PTR}    ),
  IPCCOMMAND(  quit,                1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  load_xresources,     1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  setup,               1,      {ARG_TYPE_NONE}   )
};
