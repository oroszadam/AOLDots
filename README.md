# AOLDots - Configuration for software I use

## This includes:

- Dwm
- Dmenu
- Alacritty
- Neovim
- Zsh
- An installation and management script
