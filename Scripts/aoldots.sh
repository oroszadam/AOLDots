#!/bin/sh

general_usage() {
    printf "AOLDots configuration manager\nUsage: aoldots [subcommand] [options]\n\nSubcommands:\n  xresources\n  theme\n  lang\n  help\n  welcome\n"
    exit
}

theme_usage() {
    printf "AOLDots configuration manager\nUsage: aoldots theme [options]\n\nOptions:\n  set\n  get\n  menu\n"
    exit
}

lang_usage() {
    printf "AOLDots configuration manager\nUsage: aoldots lang [options]\n\nOptions:\n  set\n  get\n  msg\n"
    exit
}

get_theme() {
    cat "$XDG_CONFIG_HOME"/aoldots/theme
}

xresources() {
    xrdb "$XDG_CONFIG_HOME"/aoldots/themes/"$(get_theme)"/xresources
}

get_lang() {
    cat "$XDG_CONFIG_HOME"/aoldots/lang
}

fatal_error() {
    printf "\033[1;31mERROR: %s\033[0m\n" "$1"
    exit 1
}

fatal_notify() {
    notify-send "$1" "$2" -a aoldots
    exit 1
}

set_theme() {
    echo "$1" > "$XDG_CONFIG_HOME"/aoldots/theme
    $0 xresources 2>/dev/null || theme_set_failed=1
    if [ "$theme_set_failed" = 1 ]; then
        if [ "$2" = "welcome" ]; then
            printf "\033[1;31m%s\033[0m\n" "$(get_msg "error.missing_theme" "$1")"
            return
        else
            fatal_error "$(get_msg "error.missing_theme" "$1")"
        fi
    fi

    sed -i "s|^colors: \*.*|colors: \*$1|" "$XDG_CONFIG_HOME"/alacritty.yml
    sed -i -e "s|^colorscheme .*|colorscheme $1|" -e "s|\\ 'colorscheme': '.*',|\\ 'colorscheme': '$( echo "$1" | sed 's|substrata|themecolor|')',|" "$XDG_CONFIG_HOME"/nvim/init.vim
    dwm-msg run_command load_xresources >/dev/null 2>/dev/null || get_msg "error.dwm_reload"
    timeout 0.25 dwm-msg run_command setup >/dev/null 2>/dev/null

    pkill dwmblocks
    nohup dwmblocks >/dev/null 2>&1 &
    [ ! "$2" = "welcome" ] && exit
}

theme_menu() {
    newtheme="$(find "$XDG_CONFIG_HOME"/aoldots/themes -type d -printf "%P\n" | tail -n +2 | dmenu -x 10 -y 10 -w 1900 -p "$(get_msg "message.active"): $(get_theme)")"
    [ -z "$newtheme" ] && exit

    $0 theme set "$newtheme" || fatal_notify "$(get_msg "error.theme_set_failed")" "$newtheme"
    notify-send "$(get_msg "message.new_theme")" "$newtheme" -a "aoldots"
}

theme() {
    case "$2" in
        set) set_theme "$3";;
        get) get_theme "$3";;
        menu) theme_menu;;
        *) theme_usage;;
    esac
}

set_lang() {
    [ ! -f "$XDG_CONFIG_HOME"/aoldots/langs/"$3".lang ] && lang_set_failed=1
    if [ "$lang_set_failed" = 1 ]; then
        if [ "$1" = "welcome" ]; then
            printf "\033[1;31m%s\033[0m\n" "$(get_msg "error.lang_not_exist" "$3")"
            return
        else
            fatal_error "$(get_msg "error.lang_not_exist" "$3")"
        fi
    fi
    echo "$3" > "$XDG_CONFIG_HOME"/aoldots/lang
}

get_msg() {
    message="$(grep "^$1=" "$XDG_CONFIG_HOME"/aoldots/langs/"$(get_lang)".lang | sed "s|$1=||")"
    shift

    vars="$(echo "$@" | tr ' ' '\n')"

    for i in $(seq "$(echo "$vars" | wc -l)"); do
        current="$(echo "$vars" | sed "$i"'q;d')"
        message="$(echo "$message" | sed "s|{$i}|$current|g")"
    done

    echo "$message"
}

lang() {
    case "$2" in
        set) set_lang "$@";;
        get) get_lang;;
        msg) shift; shift; get_msg "$@";;
        *) lang_usage;;
    esac
}

help_command() {
    case "$2" in
        keybinds) keybind_help;;
        *) general_usage;;
    esac
}

themesel() {
    while true; do
        printf "\nTheme: "
        read -r themein
        [ "$themein" = "y" ] && break
        [ -z "$themein" ] && themein="$(get_theme)"
        set_theme "$themein" "welcome"
    done
}

welcome() {
    get_msg "message.welcome"
    read -r _waiting

    get_msg "message.welcome_choose_lang"
    printf "Options: en / de / hu\nYour choice: "
    read -r langin
    [ -z "$langin" ] && langin="en"
    set_lang "welcome" "a" "$langin"

    get_msg "message.welcome_choose_theme"
    printf "%s:%s" "$(get_msg "message.welcome_available_themes")" "$(find "$XDG_CONFIG_HOME"/aoldots/themes -type d -printf "%P ")"
    themesel

    printf "%s (Y/n): " "$(get_msg "message.welcome_done")"
    read -r show_keybinds
    [ -z "$show_keybinds" ] && show_keybinds="y"

    [ "$show_keybinds" = "y" ] || [ "$show_keybinds" = "Y" ] && keybind_help
    exit 0
}

[ -z "$XDG_CONFIG_HOME" ] && XDG_CONFIG_HOME="/home/adam/.config"

case "$1" in
    xresources) xresources;;
    theme) theme "$@";;
    lang) lang "$@";;
    help) help_command "$@";;
    welcome) welcome;;
    *) general_usage;;
esac
