#!/bin/sh

# This script is supposed to be run on a base arch install

log() {
    loglevel="$1"
    message="$2"

    color="\033[0m"
    [ "$loglevel" = "WARN" ] && color="\033[0;33m"
    [ "$loglevel" = "ERROR" ] && color="\033[0;31m"

    printf "$color""[%s] %s\033[0m\n" "$loglevel" "$message"
}

fatal() {
    message="$1"

    printf "\033[1;31m[FATAL] %s\033[0m\n" "$message"
    exit 1
}

username() {
    printf "Enter your username: " 1>&1
    read -r username
    [ -z "$username" ] && username="$(username)"
    echo "$username"
}

password() {
    passwd "$username" || password "$username"
}

[ ! "$(id -u)" = "0" ] && fatal "The script needs to be executed as root"
log INFO "Running as root"
ping -c 1 github.com >/dev/null 2>&1 || fatal "Internet access is required for the setup"
log INFO "Internet is accessible"
log INFO "Step 1: Updating the system"
pacman -Syu || fatal "Updating the system failed"
log INFO "Step 2: Installing wget, curl and git"
pacman -S wget curl git --needed
log INFO "Step 3: Install packages from package list"
pacman -S - < pkglist.txt --needed
log INFO "Step 4: Creating a user"
username="$(username)"
printf "Select a default shell: /bin/bash /bin/zsh | "
read -r defshell
[ -z "$defshell" ] && defshell="/bin/zsh"
log INFO "Adding user"
useradd -m -g wheel -G users,audio,video,kvm,input -s "$defshell" "$username" || fatal "Creating the new user failed"
log INFO "Set a password for your user"
password "$username"
log INFO "Step 5: Adding user as a sudoer"
sed -i 's|^# %wheel ALL=(ALL:ALL) ALL|%wheel ALL=(ALL:ALL) ALL|g'

log INFO "Step 6: Installing libXft-bgra"
prevdir="$(pwd)"
cd /tmp
git clone https://aur.archlinux.org/libxft-bgra.git
cd libxft-bgra
makepkg -si

log INFO "Step 7: Cloning AOLDots"
cd "$prevdir"
cd ../Dwm/WM
make clean install || fatal "Building dwm failed, fix the errors to continue"
cd ../Dwmblocks
make clean install || fatal "Building dwmblocks failed, fix the errors to continue"
cd ../Statusbar
[ -z "$username" ] && username="$(printf "Since you skipped adding a user, you need to specify the user to install the statusbar scripts for: " 1>&2; read -r unme; echo "$unme")"
mkdir -p /home/"$username"/.local/bin/ 2>/dev/null
cp ./* /home/"$username"/.local/bin/